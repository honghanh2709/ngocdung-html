

// CAM NHAN KHACH HANG SECTION 1 //
// /*******************************
jQuery(document).ready(function ($) {
  "use strict";
  //  TESTIMONIALS CAROUSEL HOOK
  $('#customers-testimonials').owlCarousel({
    loop: true,
    center: true,
    items: 3,
    margin: 0,
    autoplay: true,
    dots: true,
    autoplayTimeout: 8500,
    smartSpeed: 450,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      1170: {
        items: 3
      }
    }
  });
});


// CAM NHAN KHACH HANG SECTION 2 //
// /*******************************
$(document).ready(function () {
  $('.hh-slider-items-camnhankhachhang-2').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// CAM NHAN KHACH HANG SECTION 3 //
// /*******************************
$(document).ready(function () {
  $('.hh-slider-items-camnhankhachhang-3').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// CAM NHAN KHACH HANG SECTION 4 //
// /*******************************
$(document).ready(function () {
  $('.hh-slider-items-camnhankhachhang-4').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// CAM NHAN KHACH HANG SECTION 5 //
// /*******************************
$(document).ready(function () {
  $('.hh-slider-items-camnhankhachhang-5').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 3,
        nav: false
      },
      1000: {
        items: 5,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 5,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// CAM NHAN KHACH HANG SECTION 6 //
// /*******************************
$(document).ready(function () {
  $('.hh-slider-items-camnhankhachhang-6').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// CAM NHAN KHACH HANG SECTION 7 //
// /*******************************
$(document).ready(function () {
  $('.hh-slider-items-camnhankhachhang-7').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


/* ---------- SWITCH BANG GIA SECTION 2 -----------------*/
$(".switcher input[type='checkbox']").click(function () {
  if ($(this).is(":checked")) {
    $("#equity").addClass("show");
    $("#cash").removeClass("show");
  } else if ($(this).is(":not(:checked)")) {
    $("#cash").addClass("show");
    $("#equity").removeClass("show");
  }
});

/* ---------- SWITCH BANG GIA SECTION 3 -----------------*/
$(".switcher input[type='checkbox']").click(function () {
  if ($(this).is(":checked")) {
    $("#equity01").addClass("show");
    $("#cash01").removeClass("show");
  } else if ($(this).is(":not(:checked)")) {
    $("#cash01").addClass("show");
    $("#equity01").removeClass("show");
  }
});

//<=======================JS TINH NANG 5===========================>
$(document).ready(function () {
  $('.hh-slider-items-tinhnang-5').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

//<=======================JS THU VIEN HINH ANH 5===========================>

$(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-gallery').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    // speed:500,
    // auto:true,
    // loop:true,
    // autoplay:false,
    onSliderLoad: function () {
      $('#image-gallery').removeClass('cS-hidden');
    }
  });
});


/* ---------- Popup Video section 12345 -----------------*/
$(document).ready(function () {

  // Video source getting

  var $video_src;
  $('.video-btn').click(function () {
    $video_src = $(this).data("src");
  });


  // Modal open 
  $('#videoModal').on('shown.bs.modal', function (e) {

    // Video src set to autoplay and not to show related video.
    $("#video").attr('src', $video_src + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1");
  })
  // Modal closed and stoped video
  $('#videoModal').on('hide.bs.modal', function (e) {
    $("#video").attr('src', $video_src);
  })
  // document ready  
});


/* ---------- video Section 5 -----------------*/
const overlayTrigger = document.querySelector('.overlay-launch'),
  overlay = document.querySelector('.overlay'),
  tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";

const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
let player,
  source;



function onYouTubeIframeAPIReady(evt) {
  console.log(source);
  player = new YT.Player('player', {
    height: '600px',
    width: '640'
  });
}


function launchOverlay(evt) {
  const $trigger = evt.target;
  source = $trigger.dataset.src;
  overlay.classList.toggle('_active');
  player.loadVideoById(source, 5, "large")


  if (!overlay.classList.contains('_active')) {
    player.stopVideo();
  } else {
    player.playVideo();
  }
}

overlayTrigger.addEventListener('click', launchOverlay);




