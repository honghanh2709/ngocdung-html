// Menu mobile
$(".menu-mobile i.ti-menu").click(function(){
  $('header').addClass ('show-menu');
});
$(".menu-mobile i.ti-close").click(function(){
$('header').removeClass ('show-menu');
});

$("nav > ul > li").on("click", function() {
  $("nav > ul > li").removeClass("active");
  $(this).addClass("active");
});

//show search
$(".btn-search-mb").click(function(){
  $('.top-head').addClass('show-search-mb');

});
$(".search-head .close-search").click(function(){
    $('.top-head').removeClass('show-search-mb');
    
});

// slider product HRM
$('.carousel-item', '.show-neighbors').each(function(){
  var next = $(this).next();
  if (! next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
}).each(function(){
  var prev = $(this).prev();
  if (! prev.length) {
    prev = $(this).siblings(':last');
  }
  prev.children(':nth-last-child(2)').clone().prependTo($(this));
});

// Elements Slider DOANH NGHIEP //
  $(document).ready(function() {
    $('.ss-result').owlCarousel({
      loop: true,
      margin: 10,
      autoplayHoverPause:false,
       autoplay: 6000,
      smartSpeed: 700,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        },
        1300: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  })
// Sticky head menu to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".content-head").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// Sticky box cart  to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".section-feeback-detail").height();
  var sticky = $('.sticky-cart-product'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('show-sticky-cart-product');
  if (scroll == 0)
    sticky.removeClass('show-sticky-cart-product');
});

$(document).ready(function() {
  let a = document.querySelectorAll(".counter");
  let arrays = Array.from(a); // converting in array
  
  arrays.map((items) => {
   let count = 0;
  
   function counterUp(){
     count++;
     items.innerHTML = count;
  
     if(count == items.dataset.number){
       clearInterval(stop)  // for stop increments
     }
   }
   let stop = setInterval(() => {
     counterUp()
   }, items.dataset.speed);  // (1000 / item.dataset.speed ) for ending same time in all values from counter
  });
  

})
/* ---------- video -----------------*/
const overlayTrigger = document.querySelector('.overlay-launch'),
    overlay = document.querySelector('.overlay'),
    tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    
const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
let player,
    source;



function onYouTubeIframeAPIReady(evt) {
    console.log(source);
    player = new YT.Player('player', {
        height: '600px',
        width: '640'
    });
}


function launchOverlay(evt) {
    const $trigger = evt.target;
    source = $trigger.dataset.src;
    overlay.classList.toggle('_active');
    player.loadVideoById(source, 5, "large")


    if (!overlay.classList.contains('_active')) {
        player.stopVideo();
    } else {
        player.playVideo();
    }
}

overlayTrigger.addEventListener('click', launchOverlay);

  
  
  // Elements Animation
	if($('.wow').length){
		var wow = new WOW(
		  {
			boxClass:     'wow',      // animated element css class (default is wow)
			animateClass: 'animated', // animation css class (default is animated)
			offset:       0,          // distance to the element when triggering the animation (default is 0)
			mobile:       false,       // trigger animations on mobile devices (default is true)
			live:         true       // act on asynchronously loaded content (default is true)
		  }
		);
		wow.init();
  }
  

// Elements Slider homepage
  $(document).ready(function() {
    $('.head-slider-home').owlCarousel({
      loop: true,
      margin: 10,
      autoplayHoverPause:false,
       autoplay: 6000,
      smartSpeed: 700,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        },
        1300: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  })

  // Elements section Slider cac dong san pham
  $(document).ready(function() {
    $('.slider-nhan-hang').owlCarousel({
      loop: true,
      margin: 10,
      autoplayHoverPause:false,
       autoplay: 6000,
      smartSpeed: 700,
      responsiveClass: true,
      responsive: {
        0: {
          items: 2,
          nav: true
        },
        600: {
          items: 2,
          nav: false
        },
        1000: {
          items: 3,
          nav: true,
          loop: false,
          margin: 0
        },
        1300: {
          items: 6,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  })
/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/


/* ---------- Js Slider product detail -----------------*/

$(document).ready(function() {
  $("#content-slider").lightSlider({
            loop:true,
            keyPress:true
        });
        $('#image-gallery').lightSlider({
            gallery:true,
            item:1,
            thumbItem:9,
            slideMargin: 0,
            // speed:500,
            // auto:true,
            // loop:true,
            // autoplay:false,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }  
        });
});
/* ---------- Js show full gallery -----------------*/
$(document).ready(function(){
  $(".show-gallery").click(function(){
    $(".popup-gallery").addClass("show");
  });
  $(".close-popup").click(function(){
    $(".popup-gallery").removeClass("show");
    let vid = $('#id-gallery');
    
    vid.trigger('pause')
    vid.get(0).currentTime = 0;
  });
});

// Elements Slider feedback tu chueyn gia
$(document).ready(function() {
  $('.slider-feeback-detail').owlCarousel({
    loop: true,
    margin: 10,
    // autoplayHoverPause:false,
    //  autoplay: 6000,
    // smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})

// CAU HOI THUONG GAP //
/*******************************
* ACCORDION WITH TOGGLE ICONS
*******************************/
function toggleIcon(e) {
  $(e.target)
    .prev('.panel-heading')
    .find(".more-less")
    .toggleClass('ti-plus ti-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);


/* ====== banner slider home ======== */

// $(document).ready(function() {
//   particlesJS("particles-js", {
//     "particles": {
//               "number": {
//                 "value": 250,
//                 "density": {
//                   "enable": true
//                 },
              
//               },
//               "color": {
//                 "value": ["#DBDBDB"]
//               },
//               "opacity": {
//                 "value": 1,
//                 "random": false,
//                   "anim": {
//                     "enable": true,
//                     "speed": 1,
//                     "opacity_min": 1,
//                     "sync": false
//                   }
//               },
//               "shape": {
//                 "type": "circle"
//               },
//               "size": {
//                 "value": 2.5,
//                 "random": true
//               },
//               "line_linked": {
//                 "enable": true
//               },
//               "move": {
//                 "enable": true,
//                 "speed": 2,
//                 "random": true,
//                 "direction": "none",
//                 "straight": false
//               }
//           },
//           "interactivity": {
//               "detect_on": "canvas",
//               "events": {
//                   "onhover": {
//                     "enable": true
//                   }
//                 },
//             "modes": {
//               "push": {
//                 "particles_nb": 1
//               }
//           }
//       }
//   });
// })


