$(document).ready(function(){

	$(".owl-carousel").owlCarousel({    
		loop:true,
		items:1,
		margin:0,
		stagePadding: 0,
		autoplay:false  
	});
	
	dotcount = 1;
	
	jQuery('.owl-dot').each(function() {
		jQuery( this ).addClass( 'dotnumber' + dotcount);
		jQuery( this ).attr('data-info', dotcount);
		dotcount=dotcount+1;
	});
	
	slidecount = 1;
	
	jQuery('.owl-item').not('.cloned').each(function() {
		jQuery( this ).addClass( 'slidenumber' + slidecount);
		slidecount=slidecount+1;
	});
	
	jQuery('.owl-dot').each(function() {	
		grab = jQuery(this).data('info');		
		slidegrab = jQuery('.slidenumber'+ grab +' img').attr('src');
		jQuery(this).css("background-image", "url("+slidegrab+")");  	
	});
	
	amount = $('.owl-dot').length;
	gotowidth = 100/amount;			
	jQuery('.owl-dot').css("height", gotowidth+"%");

});

$(document).ready(function() {
  $('.best-seller-slide').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})


$(".menu .mobile-menu i.ti-align-left").on("click", function() {
  $(".menu").addClass("active");
});

$(".menu .mobile-menu i.ti-close").on("click", function() {
  $(".menu").removeClass("active");
});

$("nav > ul > li").on("click", function() {
  $("nav > ul > li").removeClass("active");
  $(this).addClass("active");
});



/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            $('.select-selected').addClass('active');
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
      
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/

// Captcha
setTimeout(function() {
  
  $('.recaptcha').each(function() {
    grecaptcha.render(this.id, {
      'sitekey': '6LdVkwkUAAAAACeeETRX--v9Js0vWyjQOTIZxxeB',
      "theme":"light"
    });
  });
  
}, 2000);

/*==============================================================

==============================================================*/
$(window).scroll(function() {
  var sticky = $('.header-main-area'),
  scroll = $(window).scrollTop();
  if (scroll >= 150) {
    sticky.addClass('is-sticky');
  }
  else {
    sticky.removeClass('is-sticky');
  }
});
/*==============================================================
Slider Chi tiet bao in
==============================================================*/
$(document).ready(function(){

	$(".owl-carousel").owlCarousel({    
		loop:true,
		items:1,
		margin:0,
		stagePadding: 0,
		autoplay:false  
	});
	
	dotcount = 1;
	
	jQuery('.owl-dot').each(function() {
		jQuery( this ).addClass( 'dotnumber' + dotcount);
		jQuery( this ).attr('data-info', dotcount);
		dotcount=dotcount+1;
	});
	
	slidecount = 1;
	
	jQuery('.owl-item').not('.cloned').each(function() {
		jQuery( this ).addClass( 'slidenumber' + slidecount);
		slidecount=slidecount+1;
	});
	
	jQuery('.owl-dot').each(function() {	
		grab = jQuery(this).data('info');		
		slidegrab = jQuery('.slidenumber'+ grab +' img').attr('src');
		jQuery(this).css("background-image", "url("+slidegrab+")");  	
	});
	
	amount = $('.owl-dot').length;
	gotowidth = 100/amount;			
	jQuery('.owl-dot').css("height", gotowidth+"%");

});
/*==============================================================
Sticky menu
==============================================================*/
$(window).scroll(function(){
  var sticky = $('header'),
      scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass('menu-scroll');
  else sticky.removeClass('menu-scroll');
});

// Elements section Slider cac dong san pham
$(document).ready(function () {
  $('.slider-service').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})


