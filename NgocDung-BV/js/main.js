var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}

// Menu mobile
$(".menu-mobile i.ti-menu").click(function () {
  $('header').addClass('show-menu');
});
$(".menu-mobile i.ti-close").click(function () {
  $('header').removeClass('show-menu');
});

$("nav > ul > li").on("click", function () {
  $("nav > ul > li").removeClass("active");
  $(this).addClass("active");
});

//Search mobile
$(".btn-search-mb").click(function () {
  $('.search-head').addClass('show-search-mb');

});
$(".search-head .close-search").click(function () {
  $('.search-head').removeClass('show-search-mb');

});

// Elements section Slider tham my cong nghe cao
$(document).ready(function () {
  $('.slider-service').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements section Slider thu vien video
$(document).ready(function () {
  $('.slider-libary-video').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider ket qua truoc sau
$(document).ready(function () {
  $('.result-item').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});






// Sticky head menu to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".content-head").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// Sticky box cart  to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".section-feeback-detail").height();
  var sticky = $('.sticky-cart-product'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('show-sticky-cart-product');
  if (scroll == 0)
    sticky.removeClass('show-sticky-cart-product');
});


// Elements Animation
if ($('.wow').length) {
  var wow = new WOW(
    {
      boxClass: 'wow',      // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset: 0,          // distance to the element when triggering the animation (default is 0)
      mobile: false,       // trigger animations on mobile devices (default is true)
      live: true       // act on asynchronously loaded content (default is true)
    }
  );
  wow.init();
}


/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/


/* ---------- Js Slider product detail -----------------*/

$(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-gallery').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    // speed:500,
    // auto:true,
    // loop:true,
    // autoplay:false,
    onSliderLoad: function () {
      $('#image-gallery').removeClass('cS-hidden');
    }
  });
});

$(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-csvc').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    // speed:500,
    // auto:true,
    // loop:true,
    // autoplay:false,
    onSliderLoad: function () {
      $('#image-csvc').removeClass('cS-hidden');
    }
  });
});




// Elements section Slider doi ngu bac si
$(document).ready(function () {
  $('.slider-list-doctor').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// Elements section Slider doi ngu bac si
$(document).ready(function () {
  $('.slider-list-KH').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 2,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 2,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// Elements section Slider quy trinh thuc hien
$(document).ready(function () {
  $('.slider-list-process').owlCarousel({
    loop: true,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 6000,
    // smartSpeed: 700,
    // responsiveClass: true,
    navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

/*******************************
* ACCORDION WITH TOGGLE ICONS
*******************************/
function toggleIcon(e) {
  $(e.target)
    .prev('.panel-heading')
    .find(".more-less")
    .toggleClass('ti-plus ti-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);






// Elements section Slider co so vat chat page about
$('.carousel-item', '.show-neighbors').each(function () {
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
}).each(function () {
  var prev = $(this).prev();
  if (!prev.length) {
    prev = $(this).siblings(':last');
  }
  prev.children(':nth-last-child(2)').clone().prependTo($(this));
});



// Sticky menu about  to srcoll

$('.menu-about ul a').click(function () {
  var href = $(this).attr('href');
  var anchor = $(href).offset();
  window.scrollTo(anchor.left, anchor.top - 150);
  return false;
});

$(window).scroll(function () {
  let contentHeadHeigh = $(".height-scroll").height();
  var sticky = $('.menu-about'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('show-sticky-infrastructure');
  if (scroll == 0)
    sticky.removeClass('show-sticky-infrastructure');
});


var form = $("#steps-process");
form.steps({
  headerTag: "h6",
  bodyTag: "section",
  transitionEffect: "fade",
  titleTemplate: '<span class="step">#index#</span> #title#'
});