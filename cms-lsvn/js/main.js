// Icon-menu-sidebar
$(".menu-icon .close-sidebar").click(function () {
  $('.main-content').addClass('hide-content-left');
});

$(".menu-icon .open-sidebar").click(function () {
  $('.main-content').removeClass('hide-content-left');
});


$("nav > ul > li").on("click", function () {
  $("nav > ul > li").removeClass("active");
  $(this).addClass("active");
});


/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/
/* ---------Js show poup filter ----------*/
$(".btn-filter").click(function () {
  $(".popup-bo-loc").addClass("show-filter");
});
$(".btn-back").on("click", function () {
  $('.popup-bo-loc').removeClass('show-filter');
});


/* ---------Js Tags input ----------*/
// Js tags input

(function () {

  "use strict"


  // Plugin Constructor
  var TagsInput = function (opts) {
    this.options = Object.assign(TagsInput.defaults, opts);
    this.init();
  }

  // Initialize the plugin
  TagsInput.prototype.init = function (opts) {
    this.options = opts ? Object.assign(this.options, opts) : this.options;

    if (this.initialized)
      this.destroy();

    if (!(this.orignal_input = document.getElementById(this.options.selector))) {
      console.error("tags-input couldn't find an element with the specified ID");
      return this;
    }

    this.arr = [];
    this.wrapper = document.createElement('div');
    this.input = document.createElement('input');
    init(this);
    initEvents(this);

    this.initialized = true;
    return this;
  }

  // Add Tags
  TagsInput.prototype.addTag = function (string) {

    if (this.anyErrors(string))
      return;

    this.arr.push(string);
    var tagInput = this;

    var tag = document.createElement('span');
    tag.className = this.options.tagClass;
    tag.innerText = string;

    var closeIcon = document.createElement('a');
    closeIcon.innerHTML = '&times;';

    // delete the tag when icon is clicked
    closeIcon.addEventListener('click', function (e) {
      e.preventDefault();
      var tag = this.parentNode;

      for (var i = 0; i < tagInput.wrapper.childNodes.length; i++) {
        if (tagInput.wrapper.childNodes[i] == tag)
          tagInput.deleteTag(tag, i);
      }
    })


    tag.appendChild(closeIcon);
    this.wrapper.insertBefore(tag, this.input);
    this.orignal_input.value = this.arr.join(',');

    return this;
  }

  // Delete Tags
  TagsInput.prototype.deleteTag = function (tag, i) {
    tag.remove();
    this.arr.splice(i, 1);
    this.orignal_input.value = this.arr.join(',');
    return this;
  }

  // Make sure input string have no error with the plugin
  TagsInput.prototype.anyErrors = function (string) {
    if (this.options.max != null && this.arr.length >= this.options.max) {
      console.log('max tags limit reached');
      return true;
    }

    if (!this.options.duplicate && this.arr.indexOf(string) != -1) {
      console.log('duplicate found " ' + string + ' " ')
      return true;
    }

    return false;
  }

  // Add tags programmatically 
  TagsInput.prototype.addData = function (array) {
    var plugin = this;

    array.forEach(function (string) {
      plugin.addTag(string);
    })
    return this;
  }

  // Get the Input String
  TagsInput.prototype.getInputString = function () {
    return this.arr.join(',');
  }


  // destroy the plugin
  TagsInput.prototype.destroy = function () {
    this.orignal_input.removeAttribute('hidden');

    delete this.orignal_input;
    var self = this;

    Object.keys(this).forEach(function (key) {
      if (self[key] instanceof HTMLElement)
        self[key].remove();

      if (key != 'options')
        delete self[key];
    });

    this.initialized = false;
  }

  // Private function to initialize the tag input plugin
  function init(tags) {
    tags.wrapper.append(tags.input);
    tags.wrapper.classList.add(tags.options.wrapperClass);
    tags.orignal_input.setAttribute('hidden', 'true');
    tags.orignal_input.parentNode.insertBefore(tags.wrapper, tags.orignal_input);
  }

  // initialize the Events
  function initEvents(tags) {
    tags.wrapper.addEventListener('click', function () {
      tags.input.focus();
    });


    tags.input.addEventListener('keydown', function (e) {
      var str = tags.input.value.trim();

      if (!!(~[9, 13, 188].indexOf(e.keyCode))) {
        e.preventDefault();
        tags.input.value = "";
        if (str != "")
          tags.addTag(str);
      }

    });
  }


  // Set All the Default Values
  TagsInput.defaults = {
    selector: '',
    wrapperClass: 'tags-input-wrapper',
    tagClass: 'tag',
    max: null,
    duplicate: false
  }

  window.TagsInput = TagsInput;

})();

var tagInput1 = new TagsInput({
  selector: 'tag-post',
  duplicate: false,
  max: 10
});
tagInput1.addData(['lsvn', 'luatsuvietnam', 'tapchiluatsu'])


// Input- file with file name

jQuery("#inputFile").on('change', function () {

  var file = this.files[0].name;
  var fileTitle = jQuery(this).attr("placeholder");
  if (jQuery(this).val() !== "") {
    jQuery(this).next().text(file);
  } else {
    jQuery(this).next().text(fileTitle);
  }
});

